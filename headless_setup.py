"""
    MIT

    OpenSSDE - reads CT-dicom files to estimate patient size and extract information for choosing SSDE conversion factor
    Copyright (c) 2016, 2017 Josef Lundman & Morgan Nyberg

    This file is part of OpenSSDE.
"""
import configparser
import os


# Set default values
export2db = False
export2file = False

# Import the ini-file
config = configparser.ConfigParser()
config.read(os.path.join(os.path.dirname(__file__), 'openssde.ini'))

if config['Export']['Type'] == 'database':
    export2db = True
    engine = config['DatabaseSettings']['engine']
    database = config['DatabaseSettings']['database']
    host = config['DatabaseSettings']['host']
    port = config['DatabaseSettings']['port']
    user = config['DatabaseSettings']['user']
    password = config['DatabaseSettings']['password']
    dbconnection = True

    try:
        if engine == 'postgresql':
            import psycopg2

        conn = psycopg2.connect(database=database, user=user, password=password, host=host, port=port)
    except:
        dbconnection = False
        raise ValueError('Could not connect to the postgresql database through psycopg2.')

    if dbconnection:
        # Check that the database table for axial images exists. If not, create it
        cur = conn.cursor()
        cur.execute("""SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_name = 'openssde_axial_list')""")
        test = cur.fetchone()

        # If the database table doesn't exist, create it
        if not test[0]:
            cur.execute("""CREATE TABLE IF NOT EXISTS openssde_axial_list (
                            id BIGSERIAL,
                            SOPInstanceUID text,
                            StudyInstanceUID text,
                            SeriesInstanceUID text,
                            MaskSuccess bool,
                            ImageType text,
                            Manufacturer text,
                            Model text,
                            kVp float4,
                            TableHeight float8,
                            PatientClipped bool,
                            PatientGeometricalOffsetX float8,
                            PatientGeometricalOffsetY float8,
                            HighDensityVoxelsSeries BIGINT,
                            HighDensityVoxelsSeriesPercent BIGINT,
                            Slices INT,
                            ImageOrientation text,
                            PRIMARY KEY(id)
                            )""")
            conn.commit()

        cur.execute(
            """SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_name = 'openssde_axial_sizes')""")
        test = cur.fetchone()

        # If the database table doesn't exist, create it
        if not test[0]:
            cur.execute("""CREATE TABLE IF NOT EXISTS openssde_axial_sizes (
                                    id BIGSERIAL,
                                    AcquisitionNumber text,
                                    AxialListId BIGINT REFERENCES openssde_axial_list (id),
                                    mAs float8,
                                    SliceThickness float4,
                                    CTDIvol float8,
                                    ZPosition float8,
                                    MeanHU float8,
                                    MedianHU float8,
                                    WED float8,
                                    CF16WED float8,
                                    CF32WED float8,
                                    LAT float8,
                                    CF16LAT float8,
                                    CF32LAT float8,
                                    AP float8,
                                    CF16AP float8,
                                    CF32AP float8,
                                    CF16APLAT float8,
                                    CF32APLAT float8,
                                    EED float8,
                                    CF16EED float8,
                                    CF32EED float8,
                                    EAD float8,
                                    SinogramED float8,
                                    CF16SinogramED float8,
                                    CF32SinogramED float8,
                                    SinogramLATcm float8,
                                    SinogramLATDegree float8,
                                    CF16SinogramLAT float8,
                                    CF32SinogramLAT float8,
                                    SinogramAPcm float8,
                                    SinogramAPDegree float8,
                                    CF16SinogramAP float8,
                                    CF32SinogramAP float8,
                                    SliceCalculationStartTime float8,
                                    SliceCalculationTime float8,
                                    PRIMARY KEY(id)
                                    )""")
            conn.commit()

        # Check that the database table for axial images exists. If not, create it
        cur = conn.cursor()
        cur.execute("""SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_name = 'openssde_localizer_list')""")
        test = cur.fetchone()

        # If the database table doesn't exist, create it
        if not test[0]:
            cur.execute("""CREATE TABLE IF NOT EXISTS openssde_localizer_list (
                            id BIGSERIAL,
                            SOPInstanceUID text,
                            StudyInstanceUID text,
                            SeriesInstanceUID text,
                            AcquisitionNumber text,
                            MaskSuccess bool,
                            ViewType text,
                            Manufacturer text,
                            Model text,
                            kVp float4,
                            mAs float8,
                            PRIMARY KEY(id)
                            )""")
            conn.commit()

        # Check that the database table for axial images exists. If not, create it
        cur = conn.cursor()
        cur.execute("""SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_name = 'openssde_localizer_sizes')""")
        test = cur.fetchone()

        # If the database table doesn't exist, create it
        if not test[0]:
            cur.execute("""CREATE TABLE IF NOT EXISTS openssde_localizer_sizes (
                            id BIGSERIAL,
                            LocalizerListId BIGINT REFERENCES openssde_localizer_list (id),
                            Pos float8,
                            Size_cm float8,
                            WED_cm float8,
                            Menke_WED_cm float8,
                            CF16WED float8,
                            CF32WED float8,
                            CF16Menke float8,
                            CF32Menke float8,
                            PRIMARY KEY(id)
                            )""")
            conn.commit()
elif config['Export']['Type'] == 'file':
    export2file = True
    if not os.path.isdir(os.path.split(config['Export']['Location'])[0]):
        raise ValueError('The path to the file into which the result should be exported does not exist.')
else:
    raise ValueError('The ini-file settings for data export are not approved. The export type have to be "database" ' +
                     'or "file"')