"""
    MIT

    OpenSSDE - reads CT-dicom files to estimate patient size and extract information for choosing SSDE conversion factor
    Copyright (c) 2016, 2017 Josef Lundman & Morgan Nyberg

    This file is part of OpenSSDE.
"""
import os
import dicom
from dicom.errors import InvalidDicomError
import numpy as np


def find_all_dcm(root, logfile=None):
    """Take a file_path/folder, go through the files in the given folder and subfolder and finds all valid DICOM
    images in the path. List all valid DICOM images in a dictionary divided by modality and image type, e.g.
    CT->Axial->image.

    :param root: Path tho the main folder to look for DICOM files in
    :param logfile: Handle for logging
    :return: dcm_files (dict)
    """
    # Find all files in the directory and subdirectories
    files = [os.path.join(path, name) for path, subdirs, files in os.walk(root) for name in files]

    if logfile is not None:
        logfile.debug('Checking which files are DICOM files')
    # Check which are valid dicom files
    valid_dcm = dict()
    for f in files:
        try:
            dcm = dicom.read_file(fp=f, stop_before_pixels=True)
            if dcm.Modality in ['CT']:
                if dcm.Modality not in valid_dcm.keys():
                    valid_dcm[dcm.Modality] = dict()
                if dcm.StudyInstanceUID not in valid_dcm[dcm.Modality].keys():
                    valid_dcm[dcm.Modality][dcm.StudyInstanceUID] = dict()
                if dcm.SeriesInstanceUID not in valid_dcm[dcm.Modality][dcm.StudyInstanceUID].keys():
                    valid_dcm[dcm.Modality][dcm.StudyInstanceUID][dcm.SeriesInstanceUID] = dict()

                if len(dcm.ImageType) > 2: # Some images in my Toshiba studies have dcm.ImageType = ['DERIVED', 'SECONDARY'], so there is no dcm.ImageType[2]
                    if dcm.ImageType[2] not in valid_dcm[dcm.Modality][dcm.StudyInstanceUID][dcm.SeriesInstanceUID].keys():
                        valid_dcm[dcm.Modality][dcm.StudyInstanceUID][dcm.SeriesInstanceUID][dcm.ImageType[2]] = dict()

                    if dcm.Modality == 'CT':
                        if dcm.ImageType[2] == 'AXIAL':
                            valid_dcm[dcm.Modality][dcm.StudyInstanceUID][dcm.SeriesInstanceUID]['AXIAL']\
                                [dcm.ImagePositionPatient[2]] = {
                                'file_path': f,
                                'dcm_header': dcm
                            }
                        elif dcm.ImageType[2] == 'REFORMATTED':
                            valid_dcm[dcm.Modality][dcm.StudyInstanceUID][dcm.SeriesInstanceUID]['REFORMATTED']\
                                [dcm.ImagePositionPatient[2]] = {
                                'file_path': f,
                                'dcm_header': dcm
                            }
                        elif dcm.ImageType[2] == 'LOCALIZER':
                            valid_dcm[dcm.Modality][dcm.StudyInstanceUID][dcm.SeriesInstanceUID]['LOCALIZER']\
                                [dcm.ImagePositionPatient[2]] = {
                                'file_path': f,
                                'dcm_header': dcm
                            }
        except IOError:
            if logfile is not None:
                logfile.debug('The file "%s" was not found', f)
            print('The file ' + f + ' was not found')
        except InvalidDicomError:
            if logfile is not None:
                logfile.debug('The file "%s" is not a valid DICOM file', f)
            print('The file ' + f + ' is not a valid DICOM file')

    return valid_dcm


def import_dcm_images(ordered_paths, image_type, image_plane_size, logfile=None):
    """Take the ordered file paths. Import the DICOM objects. Return a 3D matrix for axial images and 2D for localizers.

    :param ordered_paths: The file paths in the order they should be inserted into the output image matrix
    :param image_type: The image type (AXIAL or LOCALIZER)
    :param image_plane_size: Size/shape of the image plane
    :param logfile: Handle for logging
    :return: image_matrix (numpy array)
    """
    if logfile is not None:
        logfile.debug('Importing DICOM images')
    image_matrix = None
    if image_type == 'AXIAL' or image_type == 'REFORMATTED':
        slices = len(ordered_paths)
        image_matrix = np.empty((image_plane_size[0], image_plane_size[1], slices))
        for i in range(slices):
            tmp = dicom.read_file(ordered_paths[i])
            image_matrix[:, :, i] = pixel_array(tmp)
    if image_type == 'LOCALIZER':
        slices = len(ordered_paths)
        image_matrix = np.empty((image_plane_size[0], image_plane_size[1], slices))
        for i in range(slices):
            tmp = dicom.read_file(ordered_paths[i])
            image_matrix = pixel_array(tmp)

    return image_matrix


def pixel_array(dcm, logfile=None):
    """Take the input dicom file. Extract the pixel array and convert it to type numpy.int16. Rescale slope and
    intercept if necessary.

    :param dcm: Dicom file imported through Pydicoms read_dicom()
    :param logfile: Handle for logging
    :return: The converted and correctly scaled pixel array
    """
    if logfile is not None:
        logfile.debug('Extracting pixel array, converting to numpy.int16, and rescaling slope and intercept.')
    px = dcm.pixel_array.astype(np.int16)
    if 'RescaleSlope' in dcm:
        px *= np.int16(dcm.RescaleSlope)
    if 'RescaleIntercept' in dcm:
        px += np.int16(dcm.RescaleIntercept)

    return px
